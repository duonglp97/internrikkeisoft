package chapter5;

public class Cat extends Animal1 implements CanTalk  {
	
	@Override
	public String getName() {
		String a = "kitty";
		return a;
	}
	@Override
	public void canTalk() {
		System.out.println("meo meo");
	}
	@Override	
	public void dem(int c, int d){
		System.out.println(c+d);
	}
	public static void main(String[] args) {
		Cat a = new Cat();
		System.out.println(a.getName());
		a.canTalk();
		a.dem(5, 4);
//		Animal1 a1 = new ();
//		System.out.println(a1.getName());
		
		
	}
	
}
