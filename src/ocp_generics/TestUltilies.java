package ocp_generics;

import java.util.ArrayList;
import java.util.List;

public class TestUltilies {
	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(20);
		Utilities.fill(list, 30);
		System.out.println(list);
		
//		List<?> list1 = new ArrayList<>();
//		list1.add(new Integer(10));
		
		List<Number> intList = new ArrayList<>();
		intList.add(new Integer(10)); // okay
		intList.add(new Float(10.0f)); // oops!
	
	}
}
