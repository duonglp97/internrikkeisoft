package ocp_generics;

public class BoxPrintf<T> {
	private T t;

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;
	}

	public BoxPrintf(T t) {
		super();
		this.t = t;
	}
	public String toString() {
		return "{"+t+"}";
	}
}
