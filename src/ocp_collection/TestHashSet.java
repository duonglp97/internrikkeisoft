package ocp_collection;

import java.util.HashSet;
import java.util.Set;

public class TestHashSet {
	public static void main(String[] args) {
		Set<Integer> testSet = new HashSet<>();
		testSet.add(10);
		testSet.add(20);
		testSet.add(10);
		
		System.out.println(testSet);
	}
}
