package ocp_collection;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;


public class TestArrayList {
	public static void main(String[] args) {
		List<Integer> stt = new ArrayList<Integer>();
		stt.add(10);
		stt.add(20);
		stt.add(30);
		stt.add(20);
		for (Integer i : stt) {
			System.out.println(i);
		}
		for (Iterator<Integer> sttIter = stt.iterator(); sttIter.hasNext();) {
			Integer sttLoop = sttIter.next();
			System.out.println(sttLoop);
		}
		List<Integer> testList = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		System.out.println("List ban dau: " + testList);
		for (Iterator iterator = testList.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			iterator.remove();
			
		}
		System.out.println("List con lai: " + testList);
	}
}
