package ocp_collection;

import java.util.Arrays;
import java.util.List;

public class TestListString  {
	public static void main(String[] args) {
		List<String> strings = Arrays.asList("a","b","c");
		strings.forEach(string -> System.out.println(string));;
		
		
		int a=20;
		TestLamdaExpression test = b -> System.out.println(b);
		TestLamdaExpression test2 = System.out::println;
		
		test2.test(a);
		test.test(a);
		
	}

	
}
