package ocp_collection;

import java.util.Set;
import java.util.TreeSet;

public class TestTreeSet {
	public static void main(String[] args) {
		Set<Integer> test = new TreeSet<Integer>();
		test.add(10);
		test.add(9);
		test.add(20);
		System.out.println(test);
	}
}
