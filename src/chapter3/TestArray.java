package chapter3;
import java.util.List;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class TestArray {
	public static void main(String[] args) {
		int []arr1 =  {10,20};
//		arr1 = {10,20};
		for (int i = 0; i < arr1.length; i++) {
			System.out.println(arr1[i]);
			
			
			String [] bugs = { "cricket", "beetle", "ladybug" };
			String [] alias = bugs;
			System.out.println(bugs.equals(alias)); // true
			System.out.println(bugs.toString()); // [Ljava.lang.String;@160bc7c0
			System.out.println(java.util.Arrays.toString(bugs));
//			numbers[numbers.length] = 5;
			
			
			int[][] twoD = new int[3][2];
			System.out.println(twoD.length);
			
//			ArrayList<String> l = new ArrayList();
////			int length = l.size();
//			l.remove(0);
			
			int[] random = { 6, -4, 12, 0, -10 };
			int x = 12;
			int y =Arrays.binarySearch(random, x);
			System.out.println(y);
			
			 List<Integer> list = Arrays.asList(10, 4, -1, 5);
			 Collections.sort(list);
			 Integer array[] = list.toArray(new Integer[5]);
			 System.out.println(array[3]);
			 
			 List<Integer> ages = new ArrayList<>();
			  ages.add(Integer.parseInt("5"));
			  ages.add(Integer.valueOf("6"));
			  ages.add(7);
			  ages.add(null);
			  for (int age : ages) System.out.print(age);
			  
			  
			  char[] c = new char[2];
			  int length = c.length;
			  
			  
		}
	}
}
