package chapter3;

import java.util.List;
import java.util.ArrayList;


public class TestConvertArray {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("hawk");
		list.add("robin");
		Object[] objectArray = list.toArray();
		System.out.println(objectArray.length); // 2
		String[] stringArray = list.toArray(new String[0]);
		System.out.println(stringArray.length); // 2
		list.add("duong");
		System.out.println(stringArray.length);
		System.out.println(list.size());
	}
}
