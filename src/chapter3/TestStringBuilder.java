package chapter3;

public class TestStringBuilder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder("animals");
		String sub = sb.substring(sb.indexOf("a"), sb.indexOf("al"));
		int len = sb.length();
		char ch = sb.charAt(6);
		System.out.println(sub + " " + len + " " + ch);
		
		
		StringBuilder str1 = new StringBuilder();
		StringBuilder str2 = str1.append('a');
		System.out.println(str1);
		System.out.println(str2);
		
		StringBuilder a = new StringBuilder("duong");
		a.delete(1, 3);
		System.out.println(a);
		
	}

}
