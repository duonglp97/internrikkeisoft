package chapter4;

public class TestOrderInit {
	static { add(2); }
	 static void add(int num) { System.out.print(num + " "); }
	 TestOrderInit() { add(5); }
	 static { add(4); }
	 { add(6); }
	 static { new TestOrderInit(); }
	 { add(8); }
	 public static void main(String[] args) { } 
}
