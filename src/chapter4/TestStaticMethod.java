package chapter4;

public class TestStaticMethod {
	static {
		NUM_2 = 10;
		System.out.println(TestStaticMethod.NUM_2);
	}
	public static final int NUM;
	public static final int NUM_2;
	static {
		NUM = 1000;
		System.out.println(NUM);
	}
	 
	public static void main(String[] args) {
//		System.out.println(TestStaticMethod.NUM);
//		TestStaticMethod t = new TestStaticMethod();
		
//		System.out.println(t.NUM);
	}
}
