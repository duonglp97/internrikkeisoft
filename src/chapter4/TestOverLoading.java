package chapter4;


public class TestOverLoading {
	public int tinh(int a, int b) {
		int c = a + b;
		return c;
	}
	public float tinh(float a, float b) {
		float c = a + b;
		return c;
		
		
	}
	public void in(Object a) {
		System.out.println("Object");
	}
	public void in(String a) {
		System.out.println("String");
	}
	public static void main(String[] args) {
		float a = 2.6f;
		float b = 3.7f;
		TestOverLoading test1 = new TestOverLoading();
		System.out.println(test1.tinh(a, b));
		
		test1.in(23);
	}
}
