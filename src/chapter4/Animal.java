package chapter4;

public class Animal {
	private String name;
	private boolean canSwim;
	private boolean canJump;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isCanSwim() {
		return canSwim;
	}
	public void setCanSwim(boolean canSwim) {
		this.canSwim = canSwim;
	}
	public boolean isCanJump() {
		return canJump;
	}
	public void setCanJump(boolean canJump) {
		this.canJump = canJump;
	}
	public Animal(String name, boolean canSwim, boolean canJump) {
		super();
		this.name = name;
		this.canSwim = canSwim;
		this.canJump = canJump;
	}
	
	public boolean checkSwim() {
		return this.canSwim;
	}
	public boolean checkJump() {
		return this.canJump;
	}
	
}
