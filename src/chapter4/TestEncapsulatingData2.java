package chapter4;

public class TestEncapsulatingData2 {
	TestEncapsulatingData test = new TestEncapsulatingData();
	public void setData() {
		test.testData = 100;
		System.out.println(test.testData);
		return;
	
	}
	public int howMany(boolean ...b) {
		return b.length;
	}
	public static void main(String[] args) {
		TestEncapsulatingData2 test2 = new TestEncapsulatingData2();
		test2.setData();
		
		
		System.out.println(test2.howMany(new boolean[2]));
		
	}
	
}
