package chapter4;

public class TestEncapsulatingData {
	int testData ;
	boolean c;
	public int getTestData() {
		return testData;
	}
	public void setTestData(int testData) {
		this.testData = testData;
	}
	public boolean isC() {
		return c;
	}
	public void setC(boolean c) {
		this.c = c;
	}
	
}
