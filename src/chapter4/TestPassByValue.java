package chapter4;

public class TestPassByValue {
	public static void main(String[] args) {
		int a = 8;
		int b = tinh(a);
//		System.out.println(tinh(a));
		System.out.println(b);
	}
	public static int tinh(int a) {
		a++;
		return a;
	}
}
