package chapter2;

public class WhileLoop {
	int roomInBelly = 5;
	public void eatCheese(int bitesOfCheese) {
		while (bitesOfCheese > 0 && roomInBelly > 0) {
		bitesOfCheese--;
		roomInBelly--;
		}
		System.out.println(bitesOfCheese+" pieces of cheese left");
		}
	public static void main(String[] args) {
		WhileLoop w = new WhileLoop();
		w.eatCheese(6);
		byte g = 5;
		byte e = 10;
		double f = g+e;
		System.out.println(f);
//		int x = 0;
//		 while(x++ < 10) {}
//		 String message = x > 10 ? "Greater than" : false;
//		System.out.println(message+","+x);
//		long x= 10;
//		int y = (int) (2 * x);
//		int x = 4;
//		 long y = x * 4 - x++;
//		 if(y<10) System.out.println("Too Low");
//		 else System.out.println("Just right");
//		 else System.out.println("Too High");
//		byte a = 40, b = 50;
//		 byte sum = (byte) ((byte) a + b);
//		 System.out.println(sum);
		
	}
}
